import IGlobalCryptoDTO from "../../DTO/GlobalCryptoDTO";


export const mockGlobalCryto: IGlobalCryptoDTO[] = [
    {
        coins_count: 7693,
        active_markets: 21985,
        total_mcap: 2015785757020.005,
        total_volume: 132014612220.288,
        btc_d: 39.58,
        eth_d: 18.95,
        mcap_change: -3.70,
        volume_change: -6.36,
        avg_change_percent: -0.85,
        volume_ath: 3992741953593.4854,
        mcap_ath: 2912593726674.3335
    }
]


export const mockDetailCrytoCoin = [
    {
        id: 90,
        symbol: "BTC",
        name: "Bitcoin",
        nameid: "bitcoin",
        rank: 1,
        price_usd: 42187.11,
        percent_change_24h: -2.19,
        percent_change_1h: 0.04,
        percent_change_7d: 1.62,
        market_cap_usd: 798447541013.85,
        volume24: 15500270068.70,
        volume24_native: 367417.23,
        csupply: 18926340.00,
        price_btc: 1.00,
        tsupply: 18926340,
        msupply: 21000000
    }
]

export const mockListCrytoCoins = {
    "data": [
        {
            "id": "90",
            "symbol": "BTC",
            "name": "Bitcoin",
            "nameid": "bitcoin",
            "rank": 1,
            "price_usd": "42228.10",
            "percent_change_24h": "-1.96",
            "percent_change_1h": "0.34",
            "percent_change_7d": "1.55",
            "price_btc": "1.00",
            "market_cap_usd": "799223428624.24",
            "volume24": 15497460431.02248,
            "volume24a": 13268189775.740433,
            "csupply": "18926340.00",
            "tsupply": "18926340",
            "msupply": "21000000"
        },
        {
            "id": "80",
            "symbol": "ETH",
            "name": "Ethereum",
            "nameid": "ethereum",
            "rank": 2,
            "price_usd": "3210.85",
            "percent_change_24h": "-4.09",
            "percent_change_1h": "0.48",
            "percent_change_7d": "3.31",
            "price_btc": "0.076034",
            "market_cap_usd": "382479188352.06",
            "volume24": 9116644749.15507,
            "volume24a": 7453787926.830828,
            "csupply": "119120945.00",
            "tsupply": "119120945",
            "msupply": ""
        },
        {
            "id": "2710",
            "symbol": "BNB",
            "name": "Binance Coin",
            "nameid": "binance-coin",
            "rank": 3,
            "price_usd": "475.51",
            "percent_change_24h": "-4.54",
            "percent_change_1h": "0.34",
            "percent_change_7d": "9.49",
            "price_btc": "0.011260",
            "market_cap_usd": "79315077326.01",
            "volume24": 1276256394.1089165,
            "volume24a": 1044637209.114268,
            "csupply": "166801148.00",
            "tsupply": "192443301",
            "msupply": "200000000"
        },
        {
            "id": "518",
            "symbol": "USDT",
            "name": "Tether",
            "nameid": "tether",
            "rank": 4,
            "price_usd": "1.00",
            "percent_change_24h": "0.09",
            "percent_change_1h": "-0.01",
            "percent_change_7d": "0.18",
            "price_btc": "0.000024",
            "market_cap_usd": "73678855670.74",
            "volume24": 48512954515.719925,
            "volume24a": 38982675777.735466,
            "csupply": "73578322706.00",
            "tsupply": "68261274250",
            "msupply": ""
        },
        {
            "id": "257",
            "symbol": "ADA",
            "name": "Cardano",
            "nameid": "cardano",
            "rank": 5,
            "price_usd": "1.59",
            "percent_change_24h": "12.63",
            "percent_change_1h": "3.79",
            "percent_change_7d": "38.14",
            "price_btc": "0.000038",
            "market_cap_usd": "53052591731.37",
            "volume24": 3678861465.9755526,
            "volume24a": 1649194053.6993675,
            "csupply": "33313246914.00",
            "tsupply": "32025787326.655",
            "msupply": "45000000000"
        }
    ]
}

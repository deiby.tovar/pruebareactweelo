import * as React from 'react';
import {render, RenderResult,screen} from "@testing-library/react";
import GlobalCryptoData from "../../../pages/cryptoCoinList/GlobalCryptoData";
import {Provider} from "react-redux";
import {store} from "../../../store/store";
import CryptoCoinService from "../../../services/cryptoCoinService";
import {mockGlobalCryto} from "../../mock/mockCryptoCoin";
import {getGlobalCryptoCoin} from "../../../store/slices/cryptoCoinSlice";

const renderGlobalCryptoData = (): RenderResult =>
    render(
        <Provider store={store}>
            <GlobalCryptoData />
        </Provider>
    );

describe('test GlobalCryptoData', () => {

    beforeEach(() => {
        CryptoCoinService.getGlobalCryto = jest.fn().mockResolvedValue(mockGlobalCryto[0]);
    })

    test('deberia crear el componente', () => {
        renderGlobalCryptoData();
        screen.getByText("Cryptocurrencies:")
    })

    test('deberia actualizar el state al cargar el componente', async () => {
        await store.dispatch(getGlobalCryptoCoin());
        renderGlobalCryptoData();
        expect(screen.getByTestId("coins_count").textContent).toBe('7693')
    })
})

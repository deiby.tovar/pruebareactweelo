import * as React from 'react';
import CryptoCoinService from "../../services/cryptoCoinService";
import {mockDetailCrytoCoin, mockGlobalCryto, mockListCrytoCoins} from "../mock/mockCryptoCoin";




describe('cryptoCoinService', () => {

    test('deberia obtener informacion general de las cryptomonedas', async () => {
        CryptoCoinService.getGlobalCryto = jest.fn().mockResolvedValue(mockGlobalCryto);
        const data:any = await CryptoCoinService.getGlobalCryto();
        expect(data[0].btc_d).toBe(39.58);
    })

    test('deberia obtener una lista de criptomonedas', async () => {
        CryptoCoinService.getCryptoCoins = jest.fn().mockResolvedValue(mockListCrytoCoins);
        const response:any = await CryptoCoinService.getCryptoCoins();
        const data = response.data;
        expect(data.length).toBe(5);
        expect(data[0]).toEqual(
            expect.objectContaining({symbol: 'BTC'})
        )

    })

    test('deberia obtener obtener el detalle de una criptomoneda por su id', async () => {
        CryptoCoinService.getCryptoCoin = jest.fn().mockResolvedValue(mockDetailCrytoCoin);
        const data:any = await CryptoCoinService.getCryptoCoin(90);
        expect(data.length).toBe(1);
        expect(data[0]).toEqual(
            expect.objectContaining({symbol: 'BTC'})
        )
    })
})

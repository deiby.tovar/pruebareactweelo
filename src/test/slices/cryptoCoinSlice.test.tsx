import * as React from 'react';
import {store} from "../../store/store";
import CryptoCoinService from "../../services/cryptoCoinService";
import {mockDetailCrytoCoin, mockGlobalCryto, mockListCrytoCoins} from "../mock/mockCryptoCoin";
import {getCryptoCoin, getCryptoCoins, getGlobalCryptoCoin} from "../../store/slices/cryptoCoinSlice";
import IGlobalCryptoDTO from "../../DTO/GlobalCryptoDTO";
import {ICryptoCoinDTO} from "../../DTO/CryptoCoinDTO";

describe('CryptoCoin redux state test', () => {

    beforeEach(() => {
        CryptoCoinService.getGlobalCryto = jest.fn().mockResolvedValue(mockGlobalCryto[0]);
        CryptoCoinService.getCryptoCoins = jest.fn().mockResolvedValue(mockListCrytoCoins.data);
        CryptoCoinService.getCryptoCoin = jest.fn().mockResolvedValue(mockDetailCrytoCoin[0]);
    })

    test('deberia empezar en un estado limpio', () => {
        const initialState = {
            globalCryptoCoin: undefined,
            cryptoCoins: [],
            cryptoCoin: undefined
        }
        const state = store.getState().cryptoCoin
        expect(state).toEqual(initialState)
    })

    test('deberia obtener el detalle global de las cryptomonedas', async () => {
        await store.dispatch(getGlobalCryptoCoin())
        const globalCryptoCoin:IGlobalCryptoDTO | undefined = store.getState().cryptoCoin.globalCryptoCoin;
        expect(globalCryptoCoin?.btc_d).toBe(39.58)
    })

    test('deberia obtener un listado de criptomonedas', async () => {
        await store.dispatch(getCryptoCoins())
        const cryptoCoins:ICryptoCoinDTO[] = store.getState().cryptoCoin.cryptoCoins;
        expect(cryptoCoins.length).toBe(5);
        expect(cryptoCoins[0]).toEqual(
            expect.objectContaining({symbol: 'BTC'})
        )
    })

    test('deberia obtener el detalle de una criptomomneda por su id', async () => {
        await store.dispatch(getCryptoCoin(90))
        const cryptoCoin:ICryptoCoinDTO | undefined = store.getState().cryptoCoin.cryptoCoin;
        expect(cryptoCoin?.symbol).toBe('BTC');
    })
})

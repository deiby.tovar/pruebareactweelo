import React, {useEffect} from 'react';
import {Row, Col, Container} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store/store";
import {getGlobalCryptoCoin} from "../../store/slices/cryptoCoinSlice";

const GlobalCryptoData = () => {

    const dispatch = useDispatch();
    const globalCrypto = useSelector((state: RootState) => state.cryptoCoin.globalCryptoCoin)

    useEffect(() => {
        dispatch(getGlobalCryptoCoin());
    },[]);

    return (
        <>
            <header className="row headerGlobal">
                    <Col xs={12}>
                        <Container>
                            <Row>
                                <Col xs={12}>
                                    <span className="spanHeaderGlobal">
                                        <strong>Cryptocurrencies:</strong>
                                        <span data-testid="coins_count">{globalCrypto?.coins_count}</span>
                                    </span>
                                    <span className="spanHeaderGlobal">
                                        <strong>Active markets:</strong>
                                        {globalCrypto?.active_markets}
                                    </span>
                                    <span className="spanHeaderGlobal">
                                        <strong>BTC:</strong>
                                        {globalCrypto?.btc_d}
                                    </span>
                                    <span className="spanHeaderGlobal">
                                        <strong>ETH:</strong>
                                        {globalCrypto?.eth_d}
                                    </span>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
            </header>
        </>
    );
}

export default GlobalCryptoData;

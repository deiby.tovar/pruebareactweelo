import React from 'react';
import GlobalCryptoData from "./GlobalCryptoData";
import {Container} from "react-bootstrap";
import {CryptoCoinList} from "./CryptoCoinList";

function CryptoCoins() {
    return (
        <>
            <Container fluid>
                <GlobalCryptoData></GlobalCryptoData>
                <CryptoCoinList></CryptoCoinList>
            </Container>
        </>
    );
}

export default CryptoCoins;


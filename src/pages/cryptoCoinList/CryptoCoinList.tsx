import * as React from 'react';
import {useEffect, useState} from "react";
import {ICryptoCoinDTO} from "../../DTO/CryptoCoinDTO";
import {Col, Container, Row, Table} from "react-bootstrap";
import {CoinItemList} from "../../components/CoinItemList";
import {CrytoCoinDetail} from "./CrytoCoinDetail";
import {useDispatch, useSelector} from "react-redux";
import {getCryptoCoins} from "../../store/slices/cryptoCoinSlice";

export const CryptoCoinList = () => {

    const [modalShow, setModalShow] = useState(false);

    const dispatch = useDispatch();
    const {cryptoCoins} = useSelector((state: any) => state.cryptoCoin)


    useEffect(() => {
        dispatch(getCryptoCoins())
    }, []);

    const itemList = (cryptoCoin: ICryptoCoinDTO) => {
        return (
            <CoinItemList
                cryptoCoin={cryptoCoin}
                onShow={() => setModalShow(true)}
                key={cryptoCoin.id}>
            </CoinItemList>
        )
    }

    return (
        <>
            <CrytoCoinDetail
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <Row className="mt-3">
                <Col xs={12}>
                    <Container>
                        <Row>
                            <Col xs={12} className="mb-3">
                                <h3>Top 100 cryptocurrencies</h3>
                            </Col>
                            <Col xs={12}>
                                <Table hover responsive>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Price USD</th>
                                        <th>24h %</th>
                                        <th>7d %</th>
                                        <th>Market Cap USD</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        cryptoCoins?.map((cryptoCoin: ICryptoCoinDTO) => itemList(cryptoCoin))
                                    }
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Container>
                </Col>
            </Row>
        </>
    );
};

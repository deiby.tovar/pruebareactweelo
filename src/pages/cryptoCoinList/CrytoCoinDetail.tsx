import * as React from 'react';
import {Button, Col, Modal, Row} from "react-bootstrap";
import {useSelector} from "react-redux";
import {ICryptoCoinDTO} from "../../DTO/CryptoCoinDTO";
import {number_format} from "../../utils/helpes";

export const CrytoCoinDetail = (props: any) => {

    const cryptoCoin: ICryptoCoinDTO = useSelector((state: any) => state.cryptoCoin.cryptoCoin)

    return (
        <Modal {...props}>
            {cryptoCoin && <>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {cryptoCoin.name}
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Row>
                        <Col xs={6}>
                            <span><strong>id:</strong> {cryptoCoin.id}</span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Name:</strong> {cryptoCoin.name}</span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Symbol:</strong> {cryptoCoin.symbol}</span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Rank:</strong> {cryptoCoin.rank}</span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Price usd:</strong> ${number_format(cryptoCoin.price_usd, 2, '.', ',')}</span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Percent change 1h:</strong> <span
                                className={cryptoCoin.percent_change_1h < 0 ? 'red' : 'green'}>{cryptoCoin.percent_change_1h}</span></span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Percent change 24h:</strong> <span
                                className={cryptoCoin.percent_change_24h < 0 ? 'red' : 'green'}>{cryptoCoin.percent_change_24h}</span></span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Percent change 7d:</strong> <span
                                className={cryptoCoin.percent_change_7d < 0 ? 'red' : 'green'}>{cryptoCoin.percent_change_7d}</span></span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Market cap usd:</strong> ${number_format(cryptoCoin.market_cap_usd, 2, '.', ',')}</span>
                        </Col>
                        <Col xs={6}>
                            <span><strong>Price BTC:</strong> {cryptoCoin.price_btc}</span>
                        </Col>
                    </Row>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={props.onHide}>
                        Close
                    </Button>
                </Modal.Footer>
            </>}
        </Modal>
    );
};

// @flow
import * as React from 'react';
import {number_format} from "../utils/helpes";
import {useDispatch} from "react-redux";
import {getCryptoCoin} from "../store/slices/cryptoCoinSlice";

export const CoinItemList = (props: any) => {
    const {id, rank, name, symbol, price_usd, percent_change_24h, percent_change_7d, market_cap_usd} = props.cryptoCoin;

    const dispatch = useDispatch();

    const getDetail = () => {
        dispatch(getCryptoCoin(id))
        props.onShow();
    }

    return (
        <tr onClick={() => getDetail()}>
            <td>{rank}</td>
            <td><strong>{name}</strong> <span className="symbolCrypto">{symbol}</span></td>
            <td className="numerCryto">${number_format(price_usd, 2, '.', ',')}</td>
            <td className={percent_change_24h < 0 ? 'red' : 'green'}>{percent_change_24h}</td>
            <td className={percent_change_7d < 0 ? 'red' : 'green'}>{percent_change_7d}</td>
            <td className="numerCryto">${number_format(market_cap_usd, 2, '.', ',')}</td>
        </tr>
    );
};

import IGlobalCryptoDTO from "../DTO/GlobalCryptoDTO";
import {ICryptoCoinDTO, ICryptoCoinResponseDTO} from "../DTO/CryptoCoinDTO";
import axios from "axios";


const urlBase = 'https://api.coinlore.net/api';

const CryptoCoinService = {
    getGlobalCryto: async () => {
        const dataJson = await axios.get(`${urlBase}/global/`);
        const data: IGlobalCryptoDTO[] = dataJson.data;
        return data[0];
    },
    getCryptoCoins: async () =>
    {
        const dataJson = await axios.get(`${urlBase}/tickers/`);
        const data: ICryptoCoinResponseDTO = dataJson.data;
        return data.data;
    },
    getCryptoCoin: async (id:number) =>
    {
        const dataJson = await axios.get(`${urlBase}/ticker/?id=${id}`);
        const data: ICryptoCoinDTO[] = await dataJson.data;
        return data[0];
    }
}

export default  CryptoCoinService;

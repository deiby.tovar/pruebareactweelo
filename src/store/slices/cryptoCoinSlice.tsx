import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import IGlobalCryptoDTO from "../../DTO/GlobalCryptoDTO";
import {ICryptoCoinDTO} from "../../DTO/CryptoCoinDTO";
import CryptoCoinService from "../../services/cryptoCoinService";

export interface cryptoCoinsState {
    globalCryptoCoin: IGlobalCryptoDTO | undefined;
    cryptoCoins: ICryptoCoinDTO[];
    cryptoCoin: ICryptoCoinDTO | undefined;
}

const initialState: cryptoCoinsState = {
    globalCryptoCoin: undefined,
    cryptoCoins: [],
    cryptoCoin: undefined
}

export const getGlobalCryptoCoin = createAsyncThunk(
    "cryptoCoin/getGlobalCryptoCoins",
    async () => {
        return await CryptoCoinService.getGlobalCryto();
    }
);

export const getCryptoCoins = createAsyncThunk(
    "cryptoCoin/getCryptoCoins",
    async () => {
        return await CryptoCoinService.getCryptoCoins();
    }
);

export const getCryptoCoin = createAsyncThunk(
    "cryptoCoin/getCryptoCoin",
    async (id: number) => {
        return await CryptoCoinService.getCryptoCoin(id);
    }
);


export const cryptoCoinSlice = createSlice({
    name: 'cryptoCoins',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getGlobalCryptoCoin.fulfilled, (state, action) => {
            state.globalCryptoCoin = action.payload
        })
            .addCase(getCryptoCoins.fulfilled, (state, action) => {
                state.cryptoCoins = action.payload
            })
            .addCase(getCryptoCoin.fulfilled, (state, action) => {
                state.cryptoCoin = action.payload
            })
    }
})


const {reducer} = cryptoCoinSlice;
export default reducer;

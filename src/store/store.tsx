import {configureStore} from '@reduxjs/toolkit'
import cryptoCoinReducer from './slices/cryptoCoinSlice'

const reducer = {
    cryptoCoin: cryptoCoinReducer
}

export const store = configureStore({
    reducer: reducer
})
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
